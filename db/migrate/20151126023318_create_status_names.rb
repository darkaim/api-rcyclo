class CreateStatusNames < ActiveRecord::Migration
  def change
    create_table :status_names do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
