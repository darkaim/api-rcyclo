class CreateEstablishmentWastes < ActiveRecord::Migration
  def change
    create_table :establishment_wastes do |t|
      t.integer :establishment_id
      t.integer :waste_type_id
      t.boolean :active

      t.timestamps null: false
    end
  end
end
