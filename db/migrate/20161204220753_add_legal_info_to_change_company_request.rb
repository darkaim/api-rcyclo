class AddLegalInfoToChangeCompanyRequest < ActiveRecord::Migration
  def change
  	add_column :change_company_requests, :rut, :string
  	add_column :change_company_requests, :agent, :string
  	add_column :change_company_requests, :agent_rut, :string
  end
end
