class CreateContainers < ActiveRecord::Migration
  def change
    create_table :containers do |t|
      t.integer :company_id
      t.integer :establishment_id
      t.string :title
      t.string :address
      t.text :description
      t.integer :status_id
      t.boolean :active
      t.boolean :erased
      t.integer :waste_type_id
      t.float :latitude
      t.float :longitude

      t.timestamps null: false
    end
  end
end
