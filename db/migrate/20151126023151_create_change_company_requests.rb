class CreateChangeCompanyRequests < ActiveRecord::Migration
  def change
    create_table :change_company_requests do |t|
      t.integer :company_id
      t.string :name
      t.string :address
      t.string :email
      t.boolean :validated

      t.timestamps null: false
    end
  end
end
