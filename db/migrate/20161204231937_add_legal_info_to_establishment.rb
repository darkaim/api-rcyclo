class AddLegalInfoToEstablishment < ActiveRecord::Migration
  def change
  	add_column :establishments, :rut, :string
  	add_column :establishments, :agent, :string
  	add_column :establishments, :agent_rut, :string
  end
end
