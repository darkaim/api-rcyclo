class AddNameAndAddressAndActiveAndErasedToEstablishments < ActiveRecord::Migration
  def change
    add_column :establishments, :address, :string
    add_column :establishments, :active, :boolean
    add_column :establishments, :erased, :boolean
  end
end
