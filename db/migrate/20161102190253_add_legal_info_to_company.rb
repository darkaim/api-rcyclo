class AddLegalInfoToCompany < ActiveRecord::Migration
  def change
  	add_column :companies, :rut, :string
  	add_column :companies, :agent, :string
  	add_column :companies, :agent_rut, :string
  end
end
