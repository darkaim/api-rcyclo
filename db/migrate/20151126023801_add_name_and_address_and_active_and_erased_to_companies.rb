class AddNameAndAddressAndActiveAndErasedToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :address, :string
    add_column :companies, :active, :boolean
    add_column :companies, :erased, :boolean
  end
end
