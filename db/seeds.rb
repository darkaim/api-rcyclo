# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Company.create(email:"aleccapetillo@gmail.com",password:"Niacaniaca",name:"KITEKNOLOGY",address:"UTFSM, Valparaiso, Chile")
Establishment.create(email:"aleccapetillo@hotmail.com",password:"Niacaniaca",name:"Fundación San jose",address:"Caleta Portales, Valparaiso, Chile")
Establishment.create(email:"coaniquem@gmail.com",password:"Niacaniaca",name:"Fundación Coaniquem",address:"Av. Valparaíso 161, Oficina 4, Viña del Mar, Chile")
Establishment.create(email:"cenfa@gmail.com",password:"Niacaniaca",name:"Fundación Cenfa",address:"Yungay 1827, Valparaiso, Chile")
WasteType.create(name:"Papel")
WasteType.create(name:"Plástico")
WasteType.create(name:"Vidrio")
EstablishmentWaste.create(establishment_id:1,waste_type_id:1)
EstablishmentWaste.create(establishment_id:1,waste_type_id:2)
EstablishmentWaste.create(establishment_id:2,waste_type_id:3)
EstablishmentWaste.create(establishment_id:3,waste_type_id:2)
Container.create(company_id:1,establishment_id:1,title:"Portales",address:"Caleta portales, Valparaiso, Chile",description:"uehuehuehuehu",status_id:1,active:true,waste_type_id:1)
StatusName.create(name:"Vacío")
StatusName.create(name:"Medio")
StatusName.create(name:"Lleno")