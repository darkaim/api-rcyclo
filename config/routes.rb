Rails.application.routes.draw do
  post 'establishments/new'
  get 'establishments/index'
  get 'establishments/show'
  get 'establishments/create'
  get 'establishments/update'
  get 'establishments/destroy'
  get 'establishments/containers'
  get 'establishments/signed_in'
  post 'establishments/update'
  get 'establishments/drop_out'
  get 'establishments/accept_container_request'
  get 'establishments/update_state_container'
  post 'establishments/delete_container'
  get 'establishments/return_to_rcyclo'

  post 'companies/new'
  get 'companies/index'
  get 'companies/show'
  get 'companies/edit'
  get 'companies/update'
  get 'companies/create'
  get 'companies/destroy'
  get 'companies/containers'
  get 'companies/signed_in'
  get 'companies/waste_types_all'
  get 'companies/establishments_by_waste_type'
  get 'companies/create_container_by_company_request'
  get 'companies/update_state_container'
  get 'companies/return_to_rcyclo'
  get 'companies/drop_out'
  post 'companies/modify_data'

  get 'admins/index'
  get 'admins/new_company'
  get 'admins/accept_company'
  post 'admins/accept_company'
  get 'admins/new_establishment'
  get 'admins/accept_establishment'
  post 'admins/accept_establishment'
  get 'admins/change_request'
  get 'admins/accept_change_request'
  post 'admins/accept_change_request'
  get 'admins/signed_in'

  mount_devise_token_auth_for 'Company', at: 'company_auth', skip: [:confirmation]

  mount_devise_token_auth_for 'Establishment', at: 'establishment_auth'

  mount_devise_token_auth_for 'Admin', at: 'admin_auth'

  as :admin do
    # Define routes for Admin within this block.
  end
  as :establishment do
    # Define routes for Establishment within this block.
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
