class EstablishmentWaste < ActiveRecord::Base

	belongs_to :establishment, :foreign_key => 'establishment_id' 
	belongs_to :waste_type, :foreign_key => 'waste_type_id'

end
