class Container < ActiveRecord::Base

	geocoded_by :address
	after_validation :geocode

	#validates :title, :description, :presence => true 
	#validates :title, :length => { :minimum => 5 }


	belongs_to :company, :foreign_key => :company_id
	belongs_to :establishment, :foreign_key => :establishment_id 
	belongs_to :waste_type, :foreign_key => :waste_type_id
	belongs_to :status_name, :foreign_key => :status
	
end
