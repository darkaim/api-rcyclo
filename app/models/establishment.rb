class Establishment < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  geocoded_by :address
  after_validation :geocode

	has_many :containers
	has_many :establishment_wastes
	has_many :waste_types, through: :establishment_wastes, :foreign_key => 'establishment_id'

  include DeviseTokenAuth::Concerns::User
end
