class Company < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  geocoded_by :address
  after_validation :geocode

  has_many :containers
  has_one :change_company_request

  include DeviseTokenAuth::Concerns::User
end
