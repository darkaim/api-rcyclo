class WasteType < ActiveRecord::Base

	has_many :containers
	has_many :establishment_wastes
	has_many :establishments, through: :establishment_wastes, :foreign_key => 'waste_type_id'
	
end
