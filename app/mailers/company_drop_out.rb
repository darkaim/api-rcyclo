class CompanyDropOut < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.company_drop_out.company.subject
  #
  def company(company)
    @company = company

    mail to: company.email, subject: "Se ha dado de baja de R-Cyclo"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.company_drop_out.establishment.subject
  #
  def establishment(company, establishment, containers)
    @company = company
    @establishment = establishment
    @containers = containers

    mail to: establishment.email, subject: company.name + " se ha dado de baja de R-Cyclo"
  end
end
