class ContainerFull < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.container_full.establishment.subject
  #
  def establishment(establishment, container)
    @establishment = establishment
    @container = container

    mail to: establishment.email, subject: "Un contenedor se ha llenado"
  end
end
