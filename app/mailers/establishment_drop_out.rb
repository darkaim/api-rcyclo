class EstablishmentDropOut < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.company_drop_out.company.subject
  #
  def establishment(establishment)
    @establishment = establishment

    mail to: establishment.email, subject: "Se ha dado de baja de R-Cyclo"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.company_drop_out.establishment.subject
  #
  def company(establishment, company, containers)
    @company = company
    @establishment = establishment
    @containers = containers

    mail to: company.email, subject: establishment.name + " se ha dado de baja de R-Cyclo"
  end
end
