class ContainerDeleted < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.container_deleted.company.subject
  #
  def company(company, establishment, container)
    @company = company
    @establishment = establishment
    @container = container

    mail to: company.email, subject: establishment.name + " ha eliminado un contenedor suyo"
  end
end
