class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@rcyclo.cl"
  layout 'mailer'
end
