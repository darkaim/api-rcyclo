class EstablishmentsController < ApplicationController
  before_action :authenticate_establishment!, except: [:signed_in, :new]

  def index
    render json: current_establishment
  end

  def new
    if params[:password] == params[:password_confirmation]
      Establishment.create(name: params[:name], address: params[:address], email: params[:email], rut:params[:rut], agent:params[:agent], agent_rut:params[:agent_rut], password: params[:password], address: params[:address])
    end
  end

  def return_to_rcyclo
    current_establishment.update_attribute('erased', false)
  end

  def show
  end

  def create
  end

  def update
    current_establishment.update_attributes(:name => params[:name], :email => params[:email], :address => params[:address], rut:params[:rut], agent:params[:agent], agent_rut:params[:agent_rut])
  end

  def destroy
  end

  def containers
    containers = Container.where(establishment_id: current_establishment.id, active: true, erased: false)

    containers_request = Container.where(establishment_id: current_establishment.id, active: false, erased: false)

    containers_empty = containers.where(status_id: 1).count
    containers_half = containers.where(status_id: 2).count
    containers_full = containers.where(status_id: 3).count

    containers = containers.sort_by { |cont| cont.company_id }

    render json: {
      current_establishment: current_establishment,
      containers: containers,
      containers_request: containers_request,
      containers_empty: containers_empty,
      containers_half: containers_half,
      containers_full: containers_full
    }
  end

  def accept_container_request
    container = Container.where(:id => params[:container_id])

    container.update_all(:active => true)
  end

  def signed_in
    render json: current_establishment
  end

  def update_state_container
    container = Container.find(params[:container_id])

    container.update_attribute('status_id', params[:status_id])
  end

  def delete_container
    idContainer = params[:id_container]
    tempCont = Container.find(idContainer)
    company = Company.find(tempCont.company_id)
    tempCont.update(erased: true)

    establishment = Establishment.find(tempCont.establishment_id)

    ContainerDeleted.company(company, establishment, tempCont).deliver_now

    #CompanyDropOut.container_erased_explanation(company, current_establishment).deliver_later
  end

  def drop_out
    EstablishmentDropOut.establishment(current_establishment).deliver_now

    companies = Company.joins(:containers).where(:containers => {:establishment_id => current_establishment.id}).distinct

    companies.each do |comp|
      containers = Container.where(company_id: comp.id, establishment_id: current_establishment.id, erased: false)

      EstablishmentDropOut.company(current_establishment, comp, containers).deliver_now

      containers.update_all(erased: true)
    end

    current_establishment.update_attribute('erased', true)
  end
end
