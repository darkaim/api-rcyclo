class CompaniesController < ApplicationController
  before_action :authenticate_company!, except: [:signed_in, :new]

  def index
    render json: current_company
  end

  def new
    if params[:password] == params[:password_confirmation]
      Company.create(name: params[:name], address: params[:address], email: params[:email], rut: params[:rut], agent: params[:agent], agent_rut: params[:agent_rut], password: params[:password], address: params[:address])
    end
  end

  def show
  end

  def create
  end

  def edit
    render json: current_company
  end

  def update
  end

  def destroy
  end

  def containers
    containers = Container.where(company_id: current_company.id, active: true, erased: false)

    containers_waiting = Container.where(company_id: current_company.id, active: false, erased: false)

    containers_empty = containers.where(status_id: 1).count
    containers_half = containers.where(status_id: 2).count
    containers_full = containers.where(status_id: 3).count

    render json: {
      current_company: current_company,
      containers: containers,
      containers_waiting: containers_waiting,
      containers_empty: containers_empty,
      containers_half: containers_half,
      containers_full: containers_full
    }
  end

  def waste_types_all
    waste_types = WasteType.all

    render json: {
      current_company: current_company,
      waste_types: waste_types
    }
  end

  def establishments_by_waste_type
    establishments = Establishment.joins(:establishment_wastes).where(:establishment_wastes => {:waste_type_id => params[:waste_type_id]})

    render json: {
      current_company: current_company,
      establishments: establishments,
      waste_type_id: params[:waste_type_id]
    }
  end

  def create_container_by_company_request
    waste_type = WasteType.find(params[:waste_type_id])
    establishment = Establishment.find(params[:establishment_id])

    Container.create(:company_id => current_company.id, :establishment_id => establishment.id, :waste_type_id => waste_type.id, :title => "#{establishment.name} - #{current_company.name} | #{waste_type.name}" , :address => "#{current_company.address}", :active => false, :erased => false)
  end

  def update_state_container
    container = Container.find(params[:container_id])

    container.update_attribute('status_id', params[:status_id])

    if params[:status_id].to_i == 3
      establishment = Establishment.find(container.establishment_id)

      ContainerFull.establishment(establishment, container).deliver_now
    end
  end

  def drop_out
    CompanyDropOut.company(current_company).deliver_now

    establishments = Establishment.joins(:containers).where(:containers => {:company_id => current_company.id}).distinct

    establishments.each do |est|
      containers = Container.where(company_id: current_company.id, establishment_id: est.id, erased: false)

      CompanyDropOut.establishment(current_company, est, containers).deliver_now

      containers.update_all(erased: true)
    end

    current_company.update_attribute('erased', true)
  end

  def return_to_rcyclo
    current_company.update_attribute('erased', false)
  end

  def signed_in
    render json: current_company
  end


  def modify_data
    #company = params[:company]
    comp = current_company

    if ChangeCompanyRequest.find_by_company_id(comp.id).nil?
      ChangeCompanyRequest.create(company_id:comp.id,name:params[:name], address:params[:address], email:params[:email], rut:params[:rut], agent:params[:agent], agent_rut:params[:agent_rut], validated:false)
    else
      ChangeCompanyRequest.find_by_company_id(comp.id).update(company_id:comp.id,name:params[:name], address:params[:address], email:params[:email], rut:params[:rut], agent:params[:agent], agent_rut:params[:agent_rut], validated:false)
    end

    #flash[:modify_data] = "Modificación ingresada. Debe esperar la validación del administrador para la actualización de sus datos."
    #redirect_to :controller => 'company', :action => 'index'
  end

  private

    def company_params
      params.require(:company).permit(:name, :address, :email, :rut, :agent, :agent_rut, :image)
    end

end
