class AdminsController < ApplicationController
	before_action :authenticate_admin!


	def index
		render json: current_admin
	end

	def signed_in
	    render json: admin_signed_in?
	end

	def new_company
			
		comps = Company.where(active:false,erased:false)

		render json:{   comps: comps,
						current_admin: current_admin
					}


	end

	def accept_company

		comp_id = params[:comp_id]
		company = Company.find(comp_id)
		company.update(active:true)

		#CompanyDropOut.company_accept(company).deliver_later


	end

	def new_establishment
		ests = Establishment.where(active:false, erased:false)
		render json: {  ests: ests,
						current_admin: current_admin
					 }

	end

	def accept_establishment

		est_id = params[:est_id]
		establishment = Establishment.find(est_id)
		establishment.update(active:true)

		#CompanyDropOut.establishment_accept(establishment).deliver_later

	end

	def change_request
		changes = ChangeCompanyRequest.where(validated:false)
		#comp = Array.new
		#change.each do |chan|

		#	c = Company.find(chan.company_id)
		#	comp.push(c)

		#end

		render json: {changes: changes.as_json(:include => [:company]),
						current_admin: current_admin}
	end

	def accept_change_request

		change_id = params[:change_id]
		change = ChangeCompanyRequest.find(change_id)
		change.update(validated:true)

		company = Company.find(change.company_id)
		company.update(name:change.name, address:change.address, email:change.email, rut: change.rut, agent: change.agent, agent_rut: change.agent_rut)

		#CompanyDropOut.change_request_accept(company).deliver_later

		containers = Container.where(company_id:company.id,erased:false)
		if containers.count > 0
			containers.each do |cont|
				cont.update(address:company.address)
			end
		end

		fundas=Establishment.joins(:containers).where(:containers=>{:company_id=>company.id})

		fundas.each do |fund|

			#CompanyMailer.establishment_company_explanation(fund,company).deliver_later

		end

	end

end
